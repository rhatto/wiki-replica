---
title: Email submission services
---

Email submission services consist of a server that accepts email using
authenticated SMTP for LDAP users of the `torproject.org` domain.

[[_TOC_]]

# Tutorial

In general, you can configure your email client with the following
SMTP settings:

 * Server name: `submission.torproject.org`
 * Port: `587`
 * Connection security: `STARTTLS`
 * Authentication method: `Normal password`
 * User Name: your LDAP username **without** the `@torproject.org`
   part, e.g. in my case it is `anarcat`
 * Password: LDAP email password set on the [LDAP dashboard](https://db.torproject.org/update.cgi)

`TLS` connection security, on port `465` is supported and encouraged.

## Setting an email password

To use the email submission service, you first need to set a "mail
password". For this, you need to update your account in LDAP:

 1. head towards <https://db.torproject.org/update.cgi>
 2. login with your LDAP credentials (here's how to do a [password
    reset](howto/ldap#password-reset) if you lost that)
 3. be careful to hit the "Update my info" button (not the "Full
    search")
 4. enter a new, *strong* password in the `Change mail password:`
    field (and save it in your password manager)
 5. hit the "Update..." button

What this will do is set a "mail password" in your LDAP
account. Within a few minutes, this should propagate to the submission
server, which will then be available to relay your mail to the
world. Then the next step is to configure your email client, below.

## Thunderbird configuration

In Thunderbird, you will need to add a new SMTP account in "Account
settings", "Outgoing Server (SMTP)". Then click add and fill the form
with:

 * Server name: `submission.torproject.org`
 * Port: `587`
 * Connection security: `STARTTLS`
 * Authentication method: `Normal password`
 * User Name: (your LDAP username, e.g. in my case it is `anarcat`,
 **without** the `@torproject.org` part)

`TLS` connection security, on port `465` is supported and encouraged.

Then you can set that account as the default by hitting the "Set
default" button, if only your `torproject.org` identity is configured
on the server.

If not, you need to pick your `torproject.org` account from the
"Account settings" page, then at the bottom pick the `tor` SMTP server
you have just configured.

Then on first email send you will be prompted for your email
password. You should *NOT* get a certificate warning, a real cert
(signed by Let's Encrypt) should be presented by the server.

## Apple Mail configuration

 1. Click On "Account Information"

    * User Name: (your LDAP username, e.g. in my case it is `anarcat`, **without** the `@torproject.org` part)
    * Password: the correct one.
    * Outgoing Mail Server (SMTP): `submission.torproject.org`
 
 2. Make sure that your SMTP server is set to `submission.torproject.org`
 
    Note for existing mail users: You will need to select “Add Server”
    from the SMTP pull-down menu.
 
 3. If it is, select “Server Settings”

    * For `Server Port`, put `587`
    * Select `Use Secure Socket Layer (SSL)`
    * Set `Authentication` to `Password`
    * Now, enter your correct user name and your correct password.
    * Select `OK`.

`TLS` connection security, on port `465` is supported and encouraged.

## Gmail configuration

**NOTE**: This section explains how to reconfigure a gmail account that
is _already_ configured to use a torproject.org address.  If you need to add
a new address from scratch, the process will be a little different.

 1. Click on "Settings".  (That should be the big gear icon towards
    thetop right of your window.)

 2. A "quick settings" menu should open.  Click on the "See all
    settings" button at the top of that menu.

 3. This will take you to a "Settings" page. Click on the "Accounts
    and Import" tab at the top of the page.

 4. Under "Send mail as", find the address for
    <yourname@torproject.org>, and lick the "edit info" link to the
    right of that account. (If the address isn't there, then your
    gmail account isn't set up to use your TPO account. You'll need to
    click "Add another email address" instead, and the process below
    will be slightly different.)

 5. A new "Edit email address" popup should open.  Click "Next step" on it.

 6. Finally, you'll be at a window that says "Edit email address".  Fill it
    out like this:

    - Select "Send through torproject.org SMTP servers".
    - Set "SMTP Server:" to "submission.torproject.org"
    - Set "Port:" to 587.
    - Set "Username:" to your username (_without_ "@torproject.org").
    - Set "Password:" to the email submission password that you configured.
    - Select "Use TLS", not "Use SSL".

Double-check everything, then click "Save Changes".  Gmail will try
authenticating to the SMTP server; if it's successful, then the popup window
will close and your account will be updated.

# How-to

<!-- more in-depth procedure that may require interpretation -->

## Glossary

 * **SMTP**: Simple Mail Transfer Protocol. The email protocol spoken
   between servers to deliver email. Consists of two standards,
   [RFC821](https://tools.ietf.org/html/rfc821) and [RFC5321](https://tools.ietf.org/html/rfc5321) which defined SMTP extensions, also
   known as **ESMTP**.
 * **MTA**: Mail Transport Agent. A generic SMTP server. Eugeni is
   such a server.
 * **MUA**: Mail User Agent. An "email client", a program used to
   receive, manage and send email for users.
 * **MSA** : Mail Submission Agent. An SMTP server specifically
   designed to only *receive* email.
 * **MDA**: Mail Delivery Agent. The email service actually writing
   the email to the user's mailbox. Out of scope.

This document describes the implementation of a **MSA**, although the
service will most likely also include a **MTA** functionality in that
it will actually deliver emails to targets.

## More obscure clients configuration

This section regroups email client configurations that might be a
little more exotic than commonly used software. The rule of thumb here
is that if there's a GUI to configure things, then it's not
obscure.

Also, if you know what an MTA is and are passionate about standards,
you're in the obscure category, and are welcomed to this dark corner
of the internet.

### msmtp configuration

"[msmtp](https://marlam.de/msmtp/) is an SMTP client" which "transmits a mail to an SMTP
server which takes care of further delivery". It is particularly
interesting because it supports SOCKS proxies, so you can use it to
send email over Tor.

This is how dgoulet configured his client:

    # Defaults for all accounts.
    defaults
    auth on
    protocol smtp
    tls on
    port 587

    # Account: dgoulet@torproject.org
    account torproject
    host submission.torproject.org
    from dgoulet@torproject.org
    user dgoulet
    passwordeval pass mail/dgoulet@torproject.org

### Postfix client configuration

If you run Postfix as your local Mail Transport Agent (MTA), you'll
need to do something special to route your emails through the
submission server.

First, set the following configuration in `main.cf`, by running the
following commands:

    postconf -e smtp_sasl_auth_enable=yes
    postconf -e smtp_sasl_password_maps=hash:/etc/postfix/sasl/passwd
    postconf -e smtp_sasl_security_options=
    postconf -e relayhost=submission.torproject.org:submission
    postconf -e smtp_tls_security_level=encrypt
    postfix reload

The `/etc/postfix/sasl/passwd` file holds `hostname user:pass`
configurations, one per line:

    touch /etc/postfix/sasl/passwd
    chown root:root /etc/postfix/sasl/passwd && chmod 600 /etc/postfix/sasl/passwd
    echo "submission.torproject.org user:pass" >> /etc/postfix/sasl/passwd

Then rehash that map:

    postmap /etc/postfix/sasl/passwd

Note that this method stores your plain text password on disk. Make
sure permissions on the file are limited and that you use full disk
encryption.

`may` can be used as a `security_level` if we are going to send mail
to other hosts which may not support security, but make sure that
mails are encrypted when talking to the `relayhost`, for example
through a `smtp_tls_policy_maps`.

If you want to use Tor's submission server *only* for mail sent from a
`@torproject.org` address, you'll need an [extra step](http://www.postfix.org/SASL_README.html#client_sasl_sender). This should
be in `main.cf`:

    postconf -e smtp_sender_dependent_authentication=yes
    postconf -e sender_dependent_relayhost_maps=hash:/etc/postfix/sender_relay

Then in the `/etc/postfix/sender_relay` file:

    # Per-sender provider; see also /etc/postfix/sasl_passwd.
    anarcat@torproject.org               submission.torproject.org:submission

Then rehash that map as well:

    postmap /etc/postfix/sender_relay

Make sure you do *not* change the `relayhost` (above), or reset it to
its previous value. If you have changed your [`default_transport`](http://www.postfix.org/postconf.5.html#default_transport),
you'll also need a [`sender_dependent_default_transport_maps`](http://www.postfix.org/postconf.5.html#sender_dependent_default_transport_maps) as
well:

    postconf -e sender_dependent_default_transport_maps=hash:/etc/postfix/sender_transport

With `/etc/postfix/sender_transport` looking like:

    anarcat@torproject.org               smtp:

Hash that file:

    postmap /etc/postfix/sender_transport

For debugging, you can make SMTP client sessions verbose in Postfix:

    smtp      unix  -       -       -       -       -       smtp -v

`smtp_sasl_mechanism_filter` is also very handy for debugging. For
example, you can try to force the authentication mechanism to
`cram-md5` this way.

## Pager playbook

No pager playbook has been built yet. See the [Monitoring and
testing](#monitoring-and-testing) section below for ideas on how to debug things.

## Disaster recovery

N/A. The server should be rebuildable from scratch using the Puppet
directive and does not have long-term user data. All user data is
stored in DNS or LDAP.

If email delivery starts failing, users are encouraged to go back to
the email providers they were using before this service was deployed.

# Reference

## Installation

To setup a new mail server, create a machine with the
`email::submission` role in Puppet. Ideally, it should be on a network
with a good IP reputation.

In `letsencrypt.git`, add an entry for that host's specific TLS
certificate. For example, the `submit-01.torproject.org` server has a
line like this:

    submit-01.torproject.org submit.torproject.org

Those domains are glued together in DNS with:

    submission              IN      CNAME   submit-01
    _submission._tcp        IN      SRV     0 1 587 submission

This implies there is only *one* `submission.torproject.org`, because
one cannot have multiple `CNAME` records, of course. But it should
make replacing the server transparent for end-users.

The latter SRV record is actually specified in [RFC6186](https://datatracker.ietf.org/doc/html/rfc6186), but may
not be sufficient for all automatic configuration. We do *not* go
deeper into auto-discovery, because that typically implies IMAP
servers and so on. But if we would, we could consider using [this
software which tries to support all of them](https://github.com/Monogramm/autodiscover-email-settings) (e.g. [Microsoft](https://docs.microsoft.com/en-us/exchange/architecture/client-access/autodiscover?view=exchserver-2019),
[Mozilla](https://developer.mozilla.org/en-US/docs/Mozilla/Thunderbird/Autoconfiguration/FileFormat/HowTo), [Apple](https://developer.apple.com/library/archive/featuredarticles/iPhoneConfigurationProfileRef/index.html)). For now, we'll only stick with the SRV
record.

## SLA

There is no SLA specific to this service, but mail delivery is
generally considered to be high priority. Complaints about delivery
failure should be filed as [issues in our ticket tracker](#issues) and
addressed.

## Design

The submission email service allows users to submit mail as if they
were on a `torproject.org` machine. Concretely, it is a Postfix server
which relays email to anywhere once [SASL authentication](https://en.wikipedia.org/wiki/Simple_Authentication_and_Security_Layer) is
passed.

Most of the code is glue code in Puppet, along with a small set of
patches to ud-ldap which were sent (and mostly accepted) upstream.

### Authentication

SASL authentication is delegated to a dummy Dovecot server which is
*only* used for authentication (i.e. it doesn't provide IMAP or POP
storage). Username/password pairs are deployed by [ud-ldap](howto/ldap) into
`/etc/dovecot/private/mail-passwords`.

The LDAP server stores those passwords in a `mailPassword` field and
the web interface is used to modify those passwords. Passwords are
(currently) encrypted with a salted MD5 hash because of compatibility
problems between the Perl/ud-ldap implementation and Dovecot which
haven't been resolved yet.

The "submission" port (587) is used in the documentation by default
because it is typically *less* blocked by ISP firewalls than the
"smtps" port (465), but both are supported. The TLS server is
authenticated using the regular Let's Encrypt CA (see [TLS
documentation](howto/tls)).

## Issues

There is no issue tracker specifically for this project, [File][] or
[search][] for issues in the [team issue tracker][search].

When reporting email issues, do mind the [reporting email problems](doc/reporting-email-problems)
documentation.

 [File]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new
 [search]: https://gitlab.torproject.org/tpo/tpa/team/-/issues

Project was coordinated and launched in [ticket #30608][].

## Maintainer, users, and upstream

This service is mostly written as a set of Puppet manifests. It was
built by anarcat, and is maintained by TPA. There is no upstream.

It depends on patches on `userdir-ldap` that were partially merged in
the upstream, see [LDAP docs](howto/ldap#maintainer-users-and-upstream) for details.

## Monitoring and testing

The Postfix server is monitored by Nagios, as with all servers in the
`publicmail` group. This only checks that the SMTP port is open. We do
not have end to end delivery monitoring just yet, that is part of the
[improve mail services milestone](https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/4), specifically [issue 40494](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40494).

To test delivery manually, make sure you have an `emailPassword` set
(e.g. through [update.cgi](https://db.torproject.org/update.cgi)). Then you should be able to use the
[swaks](https://tracker.debian.org/swaks) to test delivery.

This will try to relay an email through server example.net to the
example.com domain using TLS over the submission port (587) with user
name anarcat and a prompted password (`-ap -pp`).

    swaks -f anarcat@torproject.org -t anarcat@torproject.org -s submission.torproject.org -tls -p 587 -au anarcat -ap -pp

To set a new password by hand in LDAP, you can use `doveadm` to
generate a salted password. This will create a `bcrypt` password, for
example:

    doveadm pw -s BLF-CRYPT

Then copy-paste the output (minus the {} prefix) into the
`mailPassword` field in LDAP (if you want to bypass the web interface)
or the `/etc/dovecot/private/mail-passwords` file on the submission
server (if you want to bypass `ud-replicate` altogether, note that the
change might be overwritten fairly quickly). Note that [other schemes
can be used as well](https://doc.dovecot.org/configuration_manual/authentication/password_schemes/).

## Logs and metrics

The submission server is monitored like other mail servers that have
`mail_processing` enabled, which is that it has the `mtail` exporter
(`profile::prometheus::postfix_mtail_exporter`). The [Grafana
dashboard](https://grafana.torproject.org/d/Ds5BxBYGk/postfix-mtail?orgId=1&var-node=submit-01.torproject.org) should provide shiny graphs.

Mail logs are in `/var/log/mail.log` and probably systemd
journals. They contain PII like IP addresses and usernames and are
regularly purged. Mails are scanned by fail2ban to ban IP addresses
trying to bruteforce account passwords.

## Backups

No special backup of this service is required.

## Other documentation

This service was setup following some or all of those documents:

 * [Anarcat's home email setup](https://anarc.at/services/mail/)
 * [Postfix SASL howto](http://www.postfix.org/SASL_README.htm)
 * [Dovecot configuration](https://doc.dovecot.org/)
   * [VirtualUsers](https://wiki.dovecot.org/VirtualUsers)
   * [passwd file](https://doc.dovecot.org/configuration_manual/authentication/passwd_file/)
   * [password databases](https://doc.dovecot.org/configuration_manual/authentication/password_databases_passdb/)
   * [user databases](https://doc.dovecot.org/configuration_manual/authentication/user_databases_userdb/)
 * [RFC821](https://tools.ietf.org/html/rfc821) (SMTP, 1982) and [RFC5321](https://tools.ietf.org/html/rfc5321) (SMTP, 2008)
 * [RFC6186](https://datatracker.ietf.org/doc/html/rfc6186) (SRV auto-discovery)
 * [ticket 30608](https://gitlab.torproject.org/tpo/tpa/team/-/issues/30608), the original request for this service
 * [first discussion of the above](meeting/2019-06-03#new-mail-service-requests), the project was actually agreed
   upon at the Stockholm meeting in 2019

# Discussion

## Overview

The idea is to create a new server to deal with delivery problems
torproject.org email users are currently seeing. While they can
receive email through their `user@torproject.org` forwards without too
much problem, their emails often get dropped to the floor when
*sending* from that email address.

It is suspected that users are having those problems because the
originating servers are not in the `torproject.org` domain. The hope
is that setting up a new server inside that domain would help with
delivery. There's anecdotal evidence (see [this comment](https://gitlab.torproject.org/legacy/trac/-/issues/30608#note_2325187) for
example) that delivery emails from existing servers (over SSH to
`iranicum`, in that example) improves reliability of email delivery
significantly.

This project came out of [ticket #30608][], which has the launch
checklist. 

[ticket #30608]: https://bugs.torproject.org/30608

Note: [this article](https://news.purelymail.com/posts/blog/2019-06-21-deliverability-for-the-rest-of-us.html) has a good overview of deliverability issues
faced by autonomous providers, which we already face on eugeni, but
might be accentuated by this project.

## Goals

### Must have

 * basic compatibility with major clients (Thunderbird, Mail.app,
   Outlook, Gmail?)
 * delivery over secure (TLS + password) SMTP
 * credentials stored in LDAP

### Nice to have

 * automatic client configuration
 * improved delivery over current federated configuration
 * delivery reliability monitoring with major providers (e.g. hotmail,
   gmail, yahoo)
 * pretty graphs
 * formalized SSH-key delivery to avoid storing cleartext passwords on
   clients

### Non-Goals

 * 100%, infaillable, universal delivery to all providers (ie. emails
   will still be lost)
 * mailbox management (ie. no incoming email, IMAP, POP, etc)
 * spam filtering (ie. we won't check outgoing emails)
 * no DKIM, SPF, DMARC, or ARC for now, although maybe a "null" SPF
   record if it helps with delivery

## Approvals required

Approved by vegas, requested by network team, agreed with TPA at the
Stockholm meeting.

## Proposed Solution

The proposed design is to setup a new email server in the [howto/ganeti](howto/ganeti)
cluster (currently `gnt-fsn`) with the user list synchronized from
LDAP, using a new password field (named `mailPassword`). The access
would therefore be granted only to LDAP users, and LDAP accounts would
be created as needed. In the short term, LDAP can be used to modify
that password but in the mid-term, it would be modifiable through the
web interface like the `webPassword` or `rtcPassword` fields.

### Current inventory

 * active LDAP accounts: 91
 * non-LDAP forwards (to real people): 24
 * role forwards (to other @torproject.org emails): 76

Forward targets:

 * riseup.net: 30
 * gmail.com: 21
 * other: 93 (only 4 domains have more than one forward)

Delivery rate: SMTP, on eugeni, is around 0.5qps, with a max of 8qps
in the last 7 days (2019-06-06). But that includes mailing lists as
well. During that period, around 27000 emails were delivered to
@torproject.org aliases.

## Cost

Labor and `gnt-fsn` VM costs. To be detailed.

Below is an evaluation of the various Alternatives that were considered.

### External hosting cost evaluation

 * Google: 8$/mth/account? (to be verified?)
 * riseup.net: anarcat requested price quotation
 * koumbit.org:  default pricing: 100$/year on shared hosting and 50GB
   total, possibly no spam filter. 1TB disk: 500$/year. disk
   encryption would need to be implemented, quoted 2000-4000$ setup
   fee to implement it in the AlternC opensource control panel.
 * self-hosting: ~4000-500EUR setup, 5000EUR-7500EUR/year, liberal
   estimate (will probably be less)
 * [mailfence](https://mailfence.com/en/secure-business-email.jsp) 1750 setup cost and 2.5 euros per user/year

Note that the self-hosting cost evaluation is for the fully-fledged
service. Option 2, above, of relaying email, has overall negligible
costs although that theory has been questioned by members of the
sysadmin team.

### Internal hosting cost evaluation

This is a back-of-the-napkin calculation of what it would cost to host
actual email services at TPA infrastructure itself. We consider this
to be a “liberal” estimate, ie. costs would probably be less and time
estimates have been padded (doubled) to cover for errors.

Assumptions:

 * each mailbox is on average, a maximum of 10GB
 * 100 mailboxes maximum at first (so 1TB of storage required)
 * LUKS full disk encryption
 * IMAP and basic webmail (Roundcube or Rainloop)
 * “Trees” mailbox encryption out of scope for now
 
Hardware:

 * Hetzner px62nvme 2x1TB RAID-1 64GB RAM 75EUR/mth, 900EUR/yr
 * Hetzner px92 2x1TB SSD RAID-1 128GB RAM 115EUR/mth, 1380EUR/yr
 * Total hardware: 2280EUR/yr, ~200EUR setup fee

This assumes hosting the server on a dedicated server at Hetzner.  It
might be possible (and more reliable) to ensure further cost savings
by hosting it on our shared virtualized infrastructure. Calculations
for this haven’t been performed by the team, but I would guess we
might save around 25 to 50% of the above costs, depending on the
actual demand and occupancy on the mail servers.

Staff:

 * LDAP password segregation: 4 hours*
 * Dovecot deployment and LDAP integration: 8 hours
 * Dovecot storage optimization: 8 hours
 * Postfix mail delivery integration: 8 hours
 * Spam filter deployment: 8 hours
 * 100% cost overrun estimate: 36 hours
 * Total setup costs: 72 hours @ 50EUR/hr: 3600EUR one time
 
This is the most imprecise evaluation. Most email systems have been
built incrementally.  The biggest unknown is the extra labor
associated with running the IMAP server and spam filter. A few
hypothesis:

 * 1 hour a week: 52 hours @ 50EUR/hr: 2600EUR/yr
 * 2 hours a week: 5200EUR/yr

I would be surprised if the extra work goes beyond one hour a week,
and will probably be less. This also does not include 24/7 response
time, but no service provider evaluated provides that level of service
anyways.

Total:

 * One-time setup: 3800EUR (200EUR hardware, 3600EUR staff)
 * Recurrent: roughly between 5000EUR and 7500EUR/year, majority in staff

## Alternatives considered

There are three dimensions to our “decision tree”:

 1. Hosting mailboxes or only forwards: this means that instead of
    just forwarding emails to some other providers, we actually allow
    users to store emails on the server. Current situation is we only
    do forwards
 2. SMTP authentication: this means allowing users to submit email
    using a username and password over the standard SMTP (technically
    “submission”) port. This is currently not allowed also some have
    figured out they can do this over SSH already.
 3. Self-hosted or hosted elsewhere: if we host the email service
    ourselves right now or not. The current situation is we allow
    inbound messages but we do not store them. Mailbox storage is
    delegated to each individual choice of email provider, which also
    handles SMTP authentication.

Here are is the breakdown of pros and cons of each approach. Note that
there are multiple combinations of those possible, for example we
could continue not having mailboxes but allow SMTP authentication, and
delegate this to a third party. Obviously, some combinations (like no
SMTP authentication and mailboxes) are a little absurd and should be
taken with a grain of salt.

### TP full hosting: mailboxes, SMTP authentication

Pros: 

 * Easier for TPA to diagnose email problems than if email is hosted
   by an undetermined third party
 * People’s personal email is not mixed up with Tor email.
 * Easier delegation between staff on rotations
 * Control over where data is stored and how
 * Full control of our infrastructure
 * Less trust issues

Cons:

 * probably the most expensive option
 * requires more skilled staff
 * high availability harder to achieve
 * high costs

### TP not hosting mailboxes; TP hosting outgoing SMTP authentication server

Pros:

 * No data retention issues: TP not responsible for legal issues
   surrounding mailboxes contents
 * Solves delivery problem and nothing else (minimal solution)
 * We’re already running an SMTP server
 * SSH tunnels already let our lunatic-fringe do a version of this
 * Staff keeps using own mail readers (eg gmail UI) for receiving mail
 * Federated solution
 * probably the cheapest option
 * Work email cannot be accessed by TP staff

Cons:

 * SMTP-AUTH password management (admin effort and risk)
 * Possible legal requests to record outgoing mail? (SSH
   lunatic-fringe already at risk, though)
 * DKIM/SPF politics vs “slippery slope”
 * Forces people to figure out some good ISP to host their email
 * Shifts the support burden to individuals
 * Harder to diagnose email problems
 * Staff or “role” email accounts cannot be shared

### TP pays third party (riseup, protonmail, mailfence, gmail??) for full service (mailboxes, delivery)

Pros:

 * Less admin effort
 * Less/no risk to TP infrastructure (legal or technical)
 * Third party does not hold email data hostage; only handles outgoing
 * We know where data is hosted instead of being spread around

Cons:

 * Not a federated solution
 * Implicitly accepts email cartel model of “trusted” ISPs
 * Varying levels of third party data management trust required
 * Some third parties require custom software (protonmail)
 * Single point of failure.
 * Might force our users to pick a provider they dislike
 * All eggs in the same basket

### Status quo (no mailboxes, no authentication)

Pros:

 * Easy. Fast. Cheap. Pick three.

Cons:

 * Shifts burden of email debugging to users, lack of support

Details of the chosen alternative (SMTP authentication):

 * Postfix + offline LDAP authentication (current proposal)
 * Postfix + direct LDAP authentication: discarded because it might
   fail when the LDAP server goes down. LDAP server is currently not
   considered to be critical and can be restarted for maintenance
   without affecting the rest of the infrastructure.
 * reusing existing field like `webPassword` or `rtcPassword` in LDAP:
   considered a semantic violation.

See also internal Nextcloud document.

No benchmark considered necessary.
