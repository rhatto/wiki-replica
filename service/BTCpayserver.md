The machine is temporarily hosted at Lunanode while a decision is made
about where to host it.

Documentation about the current setup can be found at the following
links:

- https://docs.btcpayserver.org/Docker/#full-installation-for-technical-users

- https://docs.btcpayserver.org/LunaNodeWebDeployment/

The service is accessible at:

- https://btcpay.torproject.net/

Lunanode was chosen as a cheap and easy temporarily solution. Some
doubts remain regarding how to make sure the machine and service are
updated on a timely manner.

In the long term, given the sensitive matter of hosting a payment
service, we should have this in our tpa managed infrastructure, see
[issue tpo/tpa/team#33750](https://gitlab.torproject.org/tpo/tpa/team/-/issues/33750) for details.

TODO: integrate the service template here, and make proper docs,
especially about upgrade/backup/restore procedures.
