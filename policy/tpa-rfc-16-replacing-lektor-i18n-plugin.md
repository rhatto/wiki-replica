---
title: TPA-RFC-16: Replacing lektor-i18n-plugin
---

[[_TOC_]]

# Proposal

The proposal is for TPA/web to develop and maintain a new lektor translation plugin tentatively with the placeholder name of "new-translation-plugin". This new plugin will replace the current lektor-i18n-plugin

# Background

A note about terminology: This proposal will refer to a lektor plugin currently used by TPA named "lektor-i18n-plugin", as well as a proposed new plugin. Due to the potential confusion between these names, the currently-in-use plugin will be referred to exclusively as "lektor-i18n-plugin", and the proposed new plugin will be referred to exclusively as "new-translation-plugin", though this name is not final.

The tpo/web repos use the [lektor-i18n-plugin][1] to provide gettext-style translation for both html templates and contents.lr files. Translation is vital to our sites, and lektor-i18n-plugin seems to be the only plugin providing translation (if others exist, I haven't found them). lektor-i18n-plugin is also the source of a lot of trouble for web and TPA:

- Multiple builds are required for the plugin to work
- Python versions > 3.8.x make the plugin produce garbled POT files. For context, the current Python version at time of writing is [3.10.2][2], and 3.8.x is only receiving security updates.

Several attempts have been made to fix these pain points:

- Multiple builds: tpo/web/lego#30 shows an attempt to refactor the plugin to provide an easily-usable interface for scripts. It's had work on and off for the past 6 months, with no real progress being made.
- Garbled POT files: tpo/web/team#21 details the bug, where it occurs, and a workaround. The workaround only prevents bad translations from ending up in the site content, it doesn't fix the underlying issue of bad POT files being created. This fix hasn't been patched or upstreamed yet, so the web team is stuck on python 3.8.

Making fixes like these is hard. The lektor-i18n-plugin is one massive file, and tracing the logic and control flow is difficult. In the case of tpo/web/lego#30, the attempts at refactoring the plugin were abandoned because of the massive amount of work needed to debug small issues. lektor-i18n-plugin also seems relatively unmaintained, with only a [handful of commits in the past two and a half years][3], many made by tor contributors.

After attempting to workaround and fix some of the issues with the plugin, I've come to the conclusion that starting from scratch would be easier than trying to maintain lektor-i18n-plugin. lektor-i18n-plugin is fairly large and complex, but I don't think it needs to be. Using Lektor's [VirtualSourceObject][4] class should completely eliminate the need for multiple builds without any additional work, and using [PyBabel][5] directly (instead of `popen`ing gettext) will give us a more flexible interface, allowing for out-of-the-box support for things like [translator comments][6] and [ignoring html tags][7] that lektor-i18n-plugin seemingly doesn't support.

Using code and/or ideas from lektor-i18n-plugin will help ease the development of a new-translation-plugin. Many of the concepts behind lektor-i18n-plugin (marking contents.lr fields as translatable, databag translation, etc.) are sound, and already implemented. Even if none of the code is re-used, there's already a reference for those concepts.

By using PyBabel, VirtualSourceObject, and referencing lektor-i18n-plugin, new-translation-plugin's development and maintainence should be far easier than continuing to work around or fix lektor-i18n-plugin.

[1]: <https://github.com/numericube/lektor-i18n-plugin>
[2]: <https://www.python.org/downloads/release/python-3102/>
[3]: <https://github.com/numericube/lektor-i18n-plugin/commits/master>
[4]: <https://www.getlektor.com/docs/api/db/obj/#virtual-source-objects>
[5]: <https://babel.pocoo.org/>
[6]: <https://github.com/numericube/lektor-i18n-plugin/issues/14>
[7]: <https://github.com/numericube/lektor-i18n-plugin/issues/13>

## Plugin Design

The planned outline of the plugin looks something like this

1. The user clones a web repo, initializes submodules, and clones the correct `translation.git` branch into the `/i18n` folder (path relative to the repo root), and installs all necessary dependencies to build the lektor site
2. The user runs `lektor build` from the repo root
3. Lektor emits the `setup-env` event, which is hooked by new-translation-plugin to add the `_` function to templates
4. Lektor emits the `before-build-all` event, which is hooked by new-translation-plugin
5. new-translation-plugin regenerates the translation POT file
6. new-translation-plugin updates the PO files with the newly-regenerated POT file
7. new-translation-plugin generates a new `TranslationSource` virtual page for each page's translations, then adds the pages to the build queue

# Impact on Related Roadmaps/OKRs

The development of a new plugin could take quite a while. As a rough estimate, it could take at least a month as a *minimum* for the plugin to be completed, assuming everything goes well. Taking time away from our OKRs to work exclusively on this plugin could setback our OKR timelines by a lot. On the other hand, if we're able to complete the plugin quickly we can streamline some of our web objectives by removing issues with the current plugin.

This plugin would also greatly reduce the build time of lektor sites, since they wouldn't need to be built three times. This would make the web "OKR: make it easier for translators to contribute" about 90% complete.

# Stakeholders

The stakeholders for this RFC are members of the TPA, web, and localization teams, as well as recurring contributors to web repos and web translators.

# Approvals

This proposal needs to be explicitly approved by members of TPA, the web and localization teams, as well as operations people.

# Deadline

TBD

# Status

This proposal is currently in the `draft` state. Comments, changes, and ideas welcome by email or in the [discussion ticket](https://gitlab.torproject.org/tpo/web/team/-/issues/28).
