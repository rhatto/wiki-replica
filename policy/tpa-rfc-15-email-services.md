---
title: TPA-RFC-15: email services
---

[[_TOC_]]

Summary: deploy incoming and outgoing SPF/DKIM/DMARC checks on
torproject.org infrastructure (forcing the use of the submission
server for outgoing mail), alongside end-to-end deliverability
monitoring and a rebuild of legacy mail services to get rid of legacy
infrastructure. possibility of hosting mailboxes as a stretch goal.

# Background

In late 2021, the TPA team adopted the following first Objective and
Key Results (OKR):

> [Improve mail services][improve mail services okr]:
> 
>  1. David doesn't complain about "mail getting into spam" anymore
>  2. RT is not full of spam
>  3. we can deliver and receive mail from state.gov

This seemingly simple objective actually involves major changes to the
way email is handled on the `torproject.org` domain. Specifically, we
believe we will need to implement standards like SPF, DKIM, and DMARC
to have our mail properly delivered to large email providers, on top
of keeping hostile parties from falsely impersonating us.

## Current status

Email has traditionally been completely decentralised at Tor: while we
would support forwarding emails `@torproject.org` to other mailboxes,
we have never offered mailboxes directly, nor did we offer ways for
users to send emails themselves through our infrastructure.

This situation led to users sending email with `@torproject.org` email
addresses from arbitrary locations on the internet: Gmail, Riseup, and
other service providers were typically used to send email for
`torproject.org` users.

This changed at the end of 2021 when the new [submission service][]
came online. We still, however, have limited adoption of this service,
with only 14 users registered compared to the ~100 users in LDAP.

[submission service]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/submission

In parallel to this, we have historically not embarked in any modern
email standards like SPF, DKIM, or DMARC. But more recently, we have
added SPF records to both the Mailman and CiviCRM servers (see [ticket
40347][]).

[ticket 40347]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40347

We have also been processing DKIM headers on incoming emails on the
`bridges.torproject.org` server, but that is an exception. Finally, we
are running Spamassassin on the RT server to try to deal with the
large influx of spam on the generic support addresses (`support@`,
`info@`, etc) that the server processes. We do not process SPF records
on incoming mail in any way, which has caused problems with Hetzner
(see [issue 40539][]).

[issue 40539]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40539

We do not have any DMARC headers anywhere in DNS, but we do have
workarounds setup in Mailman for delivering email correctly when the
sender has DMARC records, since September 2021 (see [ticket 19914][]).

[ticket 19914]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/19914

We still do not offer mailboxes and this proposal does not plan to
address this, although we do have Dovecot servers deployed for
specific purposes. The GitLab and CiviCRM servers, for example, use it
for incoming email processing, and the submission server uses it for
authentication.

## Known issues

The current email infrastructure has many problems, ranging from
deliverability to monitoring. In general, people feel like their
emails are not being delivered or "getting to spam". And sometimes, in
the other direction, people simply cannot get mail from certain
domains.

Here are the currently documented problems:

 * deliverability issues: [Yahoo](https://gitlab.torproject.org/tpo/tpa/team/-/issues/34134), [state.gov](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40202), [Gmail](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40170), [Gmail again](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40149)
 * reception issues: state.gov
 * complaints about lists.tpo lacking SPF/DKIM ([issue 40347](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40347))
 * submission server incompatible with Apple Mail/Outlook (see [issue
   40586](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40586))
 * email infrastructure has multiple single points of failure ([issue
   40604](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40604))

Interlocking issues:

 * outgoing SPF deployment requires everyone to use the submission
   mail server, or at least have their server added to SPF
 * outgoing DKIM deployment requires testing and integration with DNS
   (and therefore possibly ldap)
 * outgoing DMARC deployment requires submission mail server adoption
   as well
 * SPF and DKIM require DMARC to properly function
 * DMARC requires a monitoring system to be effectively enabled

In general, we lack end-to-end deliverability tests to see if any
measures we take have an impact. See also [issue tpo/tpa/team#40494][]

[issue tpo/tpa/team#40494]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40494

## Previous evaluations

As part of the submission service launch, we did [an evaluation][]
that is complementary to this one. It evaluated the costs of hosting
various levels of our mail from "none at all" to "everything including
mailboxes", before settling on only the submission server as a
compromise.

It did not touch on email standards like this proposal does.

[an evaluation]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/submission#discussion

# Proposal

After a grace period, we progressively start adding "soft", then
"hard" SPF, DKIM, and DMARC record to the `lists.torproject.org`,
`crm.torproject.org`, `rt.torproject.org`, and, ultimately,
`torproject.org` domains.

This deployment will be paired with end to end deliverability tests
alongside "reports" analysis (from DMARC, mainly).

This assumes that, during the grace period, everyone eventually adopts
the submission server for outgoing email, or stop using their
`@torproject.org` email address for outgoing mail.

## Scope

This proposal affects SPF, DKIM, and DMARC record for outgoing mail,
on all domains managed by TPA, specifically the domain
`torproject.org` and its subdomains. It explicitly does not cover the
`torproject.net` domain.

It also affects incoming email delivery on all `torproject.org`
domains and subdomains.

The [ARC specification](http://arc-spec.org/) is currently considered out of scope,
considering that the current implementations ([OpenARC][] and
[Fastmail's authentication milter][]) are not packaged in Debian, and
no known implementation is.

[OpenARC]: https://github.com/trusteddomainproject/OpenARC
[Fastmail's authentication milter]: https://github.com/fastmail/authentication_milter

This proposal doesn't cover offering mailboxes to our users, although
it is evaluated in a separate section. It wouldn't be deployed as part
of this proposal in any case, due to time constraints, unless some
technical requirements impose it, for example compatibility with Apple
Mail or Outlook, or spam filtering improvements.

This proposal doesn't address the fate of Schleuder or Mailman (or,
for that matter, Discourse, RT, or other services that may use email
unless explicitly mentioned).

## Affected users

This affects all users which interact with `torproject.org` and its
subdomains over email. It particularly affects all "tor-internal"
users, users with LDAP accounts or forwards under `@torproject.org`.

It especially affects users which send email from their own provider
or another provider than the [submission service][]. Those users will
eventually be unable to send mail with a `torproject.org` email
address unless they take action before Q4 2022.

In general, reputation of email *not* sent using the official channels
will start to degrade some time or before Q3 2022.

## Actual changes

 a. deployment of end-to-end deliverability checks, either as Nagios
    checks or Prometheus metrics

 b. deployment of DMARC reports analysis, probably as a Prometheus
    exporter

 c. deployment of outgoing DKIM signatures and DNS records

    * watch out for [DKIM replay attacks][]

    * decide key rotation policy (how frequently, should we [publish
      private keys][])

 e. enforcement of the submission service for outgoing mail, possibly
    includes setting up a dummy IMAP server

 f. deployment of SPF and DMARC DNS records, which will impact users
    not on the submission server, which includes users with plain
    forwards and without an LDAP account, possible solutions:

    1. aliases are removed or,
    
    2. transformed into LDAP accounts or,
    
    3. they can't use them for outgoing or,
    
    4. we deploy something like [SRS][]
    
    This goes in hand with the [email policy
    problem][] which is basically the question of what service can be
    used for (e.g. forwards vs lists vs RT)

 g. inspection of incoming mail for SPF, DKIM, DMARC records, affecting
    either "reputation" (e.g. marker in mail headers) or just downright
    rejection (e.g. rejecting mail before queue)

 h. configuration of a new "mail exchanger" (MX) server with TLS
    certificates signed by a public CA, most likely Let's Encrypt for
    incoming mail, replacing a part of `eugeni`

 i. configuration of a new "mail relay" server to relay mails from
    servers that do not send their own email, replacing a part of
    `eugeni`, similar to current submission server, except with TLS
    authentication instead of password

 j. refactoring of the mail-related code in Puppet, reconfiguration of
    all servers according to the mail relay server change above, see
    [issue tpo/tpa/team#40626][]

[issue tpo/tpa/team#40626]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40626
[SRS]: https://en.wikipedia.org/wiki/Sender_Rewriting_Scheme
[email policy problem]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40404

Note: this task list must match the cost estimates below.

## Timeline

This timeline is a draft, and will be updated according to when this
proposal is adopted.

 1. Q1: start filtering incoming mail for SPF, DKIM and DMARC, first
    on `bridges.torproject.org`, then on `lists.torproject.org` and
    `torproject.org` ([ticket 40539](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40539))

 2. Q1: publish DMARC record to get reports to estimate effects of
    adding SPF records (`p=none`)
    
 3. Q1: deploy end-to-end deliverability tests with major providers
    (Google, Microsoft, Fastmail, Riseup)
    
 4. Q1: evaluate how to monitor metrics offered by [Google postmaster
    tools][] and Microsoft (see also [issue 40168][])

 3. Q1: start signing outgoing mail with DKIM, globally 

 4. Q2: everyone adopts the submission server
    
 5. Q3: once submission is adopted, add SPF records, soft (`~all`)
 
 6. Q4: deploy hard DMARC (`p=reject`) and SPF (`-all`)
 
[publish private keys]: https://blog.cryptographyengineering.com/2020/11/16/ok-google-please-publish-your-dkim-secret-keys/
[DKIM replay attacks]: https://utcc.utoronto.ca/~cks/space/blog/spam/DKIMSpamReplayAttack
[issue 40168]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40168
[Google postmaster tools]: https://postmaster.google.com

TODO: set exact deploy dates for specific records and milestones.

## Caveats

This deployment will require a lot of work on the Puppet modules,
since our current codebase around email services is a little old and
hard to modify. We will need to spend some time to refactor and
cleanup that codebase before we can move ahead with more complicated
solutions like incoming SPF checks or outgoing DKIM signatures, for
example. See [issue tpo/tpa/team#40626][] for details.

Some research work will need to be done to determine the right tools
to use to deploy the various checks on incoming mail.

Some work might be needed on `userdir-ldap` for DKIM support, although
some support for user-provided DKIM records already exists. It's
unclear if we will chose that route. At the moment, we do not plan on
giving users the privilege to deploy their own DKIM records.

## Cost estimates

Summary:

 * setup: about three months, about 25,000EUR
 * ongoing: unsure, between one day a week or a month, so about
   5,000-20,000EUR/year
 * hardware costs: negligible

This is an estimate of the time it will take to complete this project,
based on the tasks established in the [actual changes section](#actual-changes). The
process follows the [Kaplan-Moss estimation technique](https://jacobian.org/2021/may/25/my-estimation-technique/).

| Task                       | Estimate | Uncertainty | Note                                          | Total (days) |
|----------------------------|----------|-------------|-----------------------------------------------|--------------|
| a. e2e deliver. checks     | 3 days   | medium      | access to other providers uncertain           | 4.5          |
| b. DMARC reports           | 1 week   | high        | needs research                                | 10           |
| c. DKIM signing            | 3 days   | medium      | expiration policy and per-user keys uncertain | 4.5          |
| e. mandatory submission    | 3 days   | medium      | may require training                          | 4.5          |
| f. SPF/DMARC records       | 3 days   | high        | impact on forwards unclear, SRS               | 7            |
| g. incoming mail filtering | 1 week   | high        | needs research                                | 10           |
| h. new MX                  | 1 week   | high        | key part of eugeni, might be hard             | 10           |
| i. new mail relay          | 3 days   | low         | similar to current submission server          | 3.3          |
| j. Puppet refactoring      | 1 week   | high        |                                               | 10           |

This amounts to a total estimate time of 63.5 days, or about 13 weeks
or three months, full time. At 50EUR/hr, that's about 25,000EUR of
work.

This estimate doesn't cover for ongoing maintenance costs and support
associated with running the service. So far, the submission server has
yielded little support requests. After a bumpy start requiring patches
to userdir-ldap and a little documentation, things ran rather
smoothly. Since we're providing mailboxes, we are probably not going
to have a lot more documentation to write or extra hand-holding to do.

It is possible, however, that the remaining 85% of users that do *not*
currently use the submission server might require extra hand-holding,
so that's one variable that is not currently considered. We should
consider at least one person-day per month, possibly even per week,
which gives us a range of 12 to 52 days of work, for an extra cost of
5,000-20,000EUR, per year.

This estimate doesn't include hardware costs. It is hoped that,
because the changes are mostly policy and software changes, hardware
requirements will be minimal. We will need a handful of new virtual
machines (between 2 and 4, depending on how highly available we want
the service to be), and it is believed we can cover for those
requirements with the current infrastructure.

# Examples

Here we collect a few "personas" and try to see how the changes will
affect them.

> I have taken the liberty of creating mostly fictitious personas, but
> they are somewhat based in real-life people. I do not mean to
> offend, any similarity that might seem offensive is an honest
> mistake on my part which I will be happy to correct. Also note that
> I might have mixed up people together, or forgot some. If your use
> case is not mentioned here, please, please do mention it. We don't
> need to have *exactly* you here, but all your current use cases
> should be covered by one or many personas.

TODO: review the personas

## Ariel, the fundraiser

Ariel does a lot of mailing. From talking to fundraisers through her
normal inbox to doing mass newsletters to thousands of people on
CiviCRM, they get a lot of shit done and make sure we have bread on
the table at the end of the month. They're awesome and we want to make
them happy.

Email is absolutely mission critical for them. Sometimes email gets
lots and that's a huge problem. They frequently tell partners their
Gmail account address to workaround those problems. Sometimes they
send individual emails through CiviCRM because it doesn't work through
Gmail!

Their email is forwarded to Google Mail and they do *not* have an LDAP
account.

They will need to get an LDAP acount, set a mail password, and
configure the submission server as an extra identity in their Gmail
acccount. This should allow them to send email more reliably to
partners.

## Gary, the support guy

Gary is the ticket master. He eats tickets for breakfast, then files
10 more before coffee. A hundred tickets a day is just a normal day at
the office. Tickets come in through email, RT, Discourse, Telegram,
Snapchat and soon, TikTok dances.

Email is absolutely mission critical, but some days he wishes there
could be slightly less of it. He deals with a lot of spam, and
he doesn't know why, and it's annoying.

His mail forwards to Riseup and he reads his mail over Thunderbird and
sometimes webmail.

He will need to reconfigure his Thunderbird to use the submission
server after setting up an email password. The incoming mail checks
should improve the spam situation. He will need to reconfigure
riseup.net webmail account to relay mail through the submission server
(TODO: is that even possible?!) for the webmail.

## John, the external contractor

John is a freelance contractor that's really into privacy. He runs his
own relays with some cools hacks on Amazon, automatically deployed
with Terraform. They typically run their own infra in the cloud, but
for email they just got tired of fighting and moved their stuff to
Microsoft's Office 360 and Outlook.

Email is important, but not absolutely mission critical. The
submission server doesn't currently work because Outlook doesn't allow
you to add just an SMTP server.

He'll have to reconfigure his Outlook to send mail through the
submission server and use the magic empty IMAP mailbox, and not get
confused because it's always empty.

## Nancy, the fancy sysadmin

Nancy has all the elite skills in the world. She can configure a
Postfix server with her left hand while her right hand writes the
Puppet manifest for the Dovecot authentication backend. She knows her
shit. She browses her mail through a UUCP over SSH tunnel using
mutt. She runs her own mail server in her basement since 1996 (true
story).

Email is a pain in the back and she kind of hates it, but she still
believes everyone should be entitled to run their own mail server,
damnit.

Their email is, of course, hosted on their own mail server, and they
have an LDAP account.

She will have to reconfigure their Postfix server to relay mail
through the submission or relay servers, if they want to go fancy.

## Mallory, the director

Mallory also does a lot of mailing. She's on about a dozen aliases and
mailing lists from accounting to HR and other obscure ones everyone
forgot what they're for. She also deals with funders, job
applicants, contractors and staff.

Email is absolutely mission critical for her. They often fail to
contact funders and critical partners because state.gov blocks our
email (or we do? she's never quite sure, and the sysadmins don't seem
to quite know either). Sometimes, she gets told through Linked in that
a job application failed, because mail bounced at Gmail.

She has an LDAP account and it forwards to Gmail. She uses Apple
Mail to read their mail.

For her Mac, she'll need to configure the submission server *and* the
magic empty IMAP inbox, and not get confused that it's empty.

The new mail relay servers should be able to receive mail state.gov
properly. Because of the better reputation related to the new
SPF/DKIM/DMARC records, mail should bounce less (but may sometimes end
up in spam) at Gmail.

## Orpheus, the developer

Orpheus doesn't particular like or dislike email, but sometimes has to
use it to talk to people instead of compilers. They sometimes have to
talk to funders (`#grantlife`) and researchers and mailing lists, and
that often happens over email. Sometimes email is used to get
important things like ticket updates from GitLab or security
disclosures from third parties. They don't like when mail go to spam
and they have opinions on how things should be done.

They have an LDAP account and it forwards to their self-hosted mail
server on a OVH virtual machine.

Email is not mission critical, but it's pretty damn annoying when it
doesn't work.

They will have to reconfigure their mail server to relay mail through
the submission or relay servers, if they manage to convince the
sysadmin it's better that way.

# Other alternatives

## Mailboxes (+2500€/year)

The above proposal doesn't include offering mailboxes like IMAP or
POP3 services. This is considered cost-prohibitive at this point and
not something anyone has expressed a lot of interest in so far. 

It could nevertheless be necessary to solve certain problems, for
example the compatibility with Apple Mail or Outlook. It's also
possible that implementing mailboxes could help improve spam filtering
capabilities, which are after all necessary to ensure good reputation
with hosts we relay mail to. Finally, it's possible that we will not
be able to make "hard" decisions about policies like SPF, DKIM, or
DMARC and would be force to implement a "rating" system for incoming
mail, which would be difficult to deploy without user mailboxes,
especially for feedback loops.

The main cost estimate (above) therefore do not include costs specific
to mailboxes. In [the submission service hosting cost evaluation](howto/submission#internal-hosting-cost-evaluation),
the hardware costs related to mailboxes were evaluated at about
2500EUR/year with a 200EUR setup fee.

Staff costs for mailboxes are estimated to be *smaller* than the mail
services so far described here, since we already have Dovecot servers
setup and it would be "simply" a matter of deploying new
servers. There's a lot of uncertainty regarding incoming email
filtering, but that is a problem we need to solve in the current setup
anyways, so we don't believe the extra costs of this would be
significant. At worst, training would require extra server resources
and staff time for deployment. User support might require more time
than with a plain forwarding setup, however.

Therefore, it is estimated that deploying mailboxes would require an
extra 2 weeks setup time, with high uncertainty. Ongoing costs would
probably be similar to the forwarding setup, with high uncertainty as
well.

## External hosting

Other service providers have been contacted to see if it would be
reasonable to host with them. This section details those options.

All of those service providers come with significant caveats:

 * most of those may not be able to take over *all* of our email
   services. services like RT, GitLab, Mailman, CiviCRM or Discourse
   require their own mail services and may not necessarily be possible
   to outsource, particularly for mass mailings like Mailman or
   CiviCRM

 * there is a privacy concern in hosting our emails elsewhere: unless
   otherwise noted, all email providers keep mail in clear text which
   makes it accessible to hostile or corrupt staff, law enforcement,
   or external attackers

Therefore most of those solutions involve a significant compromise
(very large in the case of commercial providers) in terms of
privacy.

The costs here also do not take into account the residual maintenance
cost of the email infrastructure that we'll have to deal with if the
provider only offers a partial solution to our problems, so all of
those estimates are under-estimates, unless otherwise noted.

### Greenhost: ~1600€/year, negotiable

We had a quote from Greenhost for 129€/mth for a Zimbra frontend with
a VM for mailboxes, DKIM, SPF records and all that jazz. The price
includes an office hours SLA.

### Riseup

Riseup already hosts a significant number of email accounts by virtue
of being the target of `@torproject.org` forwards. During the [last
inventory](howto/submission#current-inventory), we found that, out of 91 active LDAP accounts, 30 were
being forwarded to `riseup.net`, so about 30%.

Riseup supports webmail, IMAP, and, more importantly, encrypted
mailboxes. While it's possible that an hostile attacker or staff could
modify the code to inspect a mailbox's content, it's leagues ahead of
most other providers in terms of privacy.

Riseup's prices are not public, but they may be close to "market"
prices quoted below.

### Gandi: 480$-2400$/year

Gandi, the DNS provider, also offers [mailbox services](https://www.gandi.net/en-US/domain/email) which are
priced at 0.40$/user-month (3GB mailboxes) or 2.00$/user-month (50GB).

It's unclear if we could do mass-mailing with this service.

### Google: 10,000$/year

Google were not contacted directly, but [their promotional site](https://workspace.google.com/solutions/new-business/)
says it's "Free for 14 days, then $7.80 per user per month", which,
for tor-internal (~100 users), would be 780$/month or ~10,000USD/year.

We probably wouldn't be able to do mass mailing with this service.

### Fastmail: 6,000$/year

Fastmail were not contacted directly but [their pricing page](https://www.fastmail.com/pricing/) says
about 5$USD/user-month, with a free 30-day trial. This amounts to
500$/mth or 6,000$/year.

It's unclear if we could do mass-mailing with this service.

### Mailcow: 480€/year

[Mailcow](https://mailcow.email/) is interesting because they actually are based on a [free
software stack](https://github.com/mailcow/mailcow-dockerized) (based on PHP, Dovecot, Sogo, rspamd, postfix,
nginx, redis, memcached, solr, Oley, and Docker containers). They
offer a [hosted service](https://www.servercow.de/mailcow?lang=en#managed) for 40€/month, with a 100GB disk quota and
no mailbox limitations (which, in our case, would mean 1GB/user).

We also get full admin access to the control panel and, given their
infrastructure, we could self-host if needed. Integration with our
current services would be, however, tricky.

It's there unclear if we could do mass-mailing with this service.

### Mailfence: 2,500€/year, 1750€ setup

The [mailfence business page](https://mailfence.com/en/secure-business-email.jsp) doesn't have prices but last time we
looked at this, it was a 1750€ setup fee with 2.5€ per user-year.

It's unclear if we could do mass-mailing with this service.

## Status quo

The current status quo is also an option. But it is our belief that it
will lead to further and further problem in deliverability. We already
have a lot of problems delivering mail to various providers, and it's
hard to diagnose issues because anyone can currently send mail
masquerading as us from anywhere.

There might be *other* solutions than the ones proposed here, but we
haven't found any good ways of solving those issues without radically
changing the infrastructure so far.

If anything, if things continue as they are, people are going to use
their `@torproject.org` email address less and less, and we'll
effectively be migrating to external providers, but delegating that
workload to individual volunteers and workers. The mailing list and,
more critically, support and promotional tools (RT and CiviCRM)
services will become less and less effective in actually delivering
emails in people's inbox and, ultimately, this will hurt our capacity
to help our users and raise funds that are critical to the future of
the project.

# Deadline

TODO.

# Status

This proposal is currently in the `draft` state.

# References

 * this work is part of the [improve mail services OKR][], part of the
   [2022 roadmap][], Q1/Q2
 * specifically, the draft of this proposal was established and
   discussed in [make a plan regarding mail standards (DKIM,SPF,
   DMARC)][]
 * the [submission service][] is the previous major undertaking to fix
   related issues to this project, and has a proposal that touches on
   some of those issues as well

[2022 roadmap]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2022
[improve mail services OKR]: https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/4
[make a plan regarding mail standards (DKIM,SPF, DMARC)]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40363

# Appendix

## Other experiences from survey

anarcat did a survey of an informal network he's a part of, and here
are the anonymized feedback. Out 9 surveyed groups, 3 are outsourcing
to either [Mailcow](https://mailcow.email/), Gandi, or Fastmail. Of the remaining 6:

 * filtering:
   * Spamassassin: 3
   * rspamd: 3
 * DMARC: 3
 * outgoing:
   * SPF: 3
   * DKIM: 2
   * DMARC: 3
   * ARC: 1
 * SMTPS: 4
   * Let's Encrypt: 4
   * MTA-STS: 1
   * DANE: 2
 * mailboxes: 4, mostly recommending Dovecot

here's a detailed listing

### Org A

 * Spamassassin: x
 * RBL: x
 * DMARC: x (quarantine, not reject)
 * SMTPS: LE
 * Cyrus: x (but suggests dovecot)

### Org B

 * used to self-host, migrated to 

### Org C

 * SPF: x
 * DKIM: soon
 * Spamassassin: x (also grades SPF, reject on mailman)
 * ClamAV: x
 * SMTPS: LE, tries SMTPS outgoing
 * Dovecot: x

### Org D

 * used to self-host, migrated to Gandi

### Org E

 * SPF, DKIM, DMARC, ARC, outbound and inbound
 * rspamd
 * SMTPS: LE + DANE
 * Dovecot

### Org F

 * SPF, DKIM
 * DMARC on lists
 * Spamassassin
 * SMTPS: LE + DANE (which triggered some outages)
 * MTA-STS
 * Dovecot

### Org G

 * no SPF/DKIM/etc
 * rspamd

### Org H

 * migrated to fastmail

### Org I

 * self-hosted in multiple locations
 * rspamd
 * no SPF/DKIM/DMARC outgoing
